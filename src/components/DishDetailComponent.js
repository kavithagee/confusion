import React, { Component } from 'react';
import { Card, CardBody, CardText, CardImg, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom'

class DishDetail extends Component {

    componentDidMount() {
        console.log("Component did mount");
    }
    componentDidUpdate() {
        console.log("Component did update");
    }
    render(props) {
        const dish = this.props.dish;
        if (dish != null) {
            const comments = this.props.comments.map((comment) => {
                return (
                    <div key={comment.id}>
                        <p>{comment.comment}
                            <br/>-- {comment.author}, {new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
                    </div>
                );
            });
            return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{dish.name}</h3>
                        <hr />
                    </div>
                </div>
            <div className="row">
            <div className="col-12 col-md-5 m-1" >
            <Card>
                <CardImg width="100%" src={dish.image} alt={dish.name}/>
                <CardBody>
                    <CardTitle>{dish.name}</CardTitle>
                    <CardText>{dish.description}</CardText>
                </CardBody>
            </Card>
            </div>
            <div>
                <CardTitle>Comments</CardTitle>
                {comments}
            </div>
            </div>
            </div>)
        } else {
            return (<div></div>)
        }

    }
}

export default DishDetail;